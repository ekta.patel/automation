package ExampletestNg;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;

public class TestDataProvider {
  @Test(dataProvider = "LoginCredentials")
  public void logIn_Test(String userId, String pass) throws InterruptedException {
	  driver.findElement(By.xpath("//tbody/tr[1]/td[2]/input[1]")).clear();
	  driver.findElement(By.xpath("//tbody/tr[2]/td[2]/input[1]")).clear();
	  driver.findElement(By.xpath("//tbody/tr[1]/td[2]/input[1]")).sendKeys(userId);
	  driver.findElement(By.xpath("//tbody/tr[2]/td[2]/input[1]")).sendKeys(pass);
	  driver.findElement(By.xpath("//tbody/tr[3]/td[2]/input[1]")).click();
	  Thread.sleep(5000);
	  String alt = driver.switchTo().alert().getText();
	  driver.switchTo().alert().accept();
	  System.out.println(alt);
  }

  @DataProvider
  public Object[][] LoginCredentials() {
       Object[][] cred = new Object[2][2];

       cred[0][0] = "userid1";
       cred[0][1] = "pass1";

       cred[1][0] = "userid2";
       cred[1][1] = "pass2";
       return cred;
    }

  public WebDriver driver;
  @BeforeClass
  public void beforeClass() {
	  System.setProperty("webdriver.chrome.driver", "E:\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe");
	  driver = new ChromeDriver();
	  driver.manage().window().maximize();
	  driver.get("http://www.demo.guru99.com/V4/");
	  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @AfterClass
  public void afterClass() {
	  driver.close();
  }

}
